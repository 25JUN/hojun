<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 
class Ex_model_8 extends CI_model {
 
 public function get()
 {
  //$this->db->select 생략시 모든 칼럼을 가져옴
 
  $this->db->order_by( "birthday", "ASC" );
  
  $this->db->limit(3);
 
  return $this->db->get( "sql_practice" );
 }
 
}
 
?>