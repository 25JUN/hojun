	
<!--Controller-->
<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 
class Mytest extends CI_controller {
 
 public function view(){
  $this->load->view( "test_form_view" );
 }
 
 public function pocketmon(){
  $this->load->model("model_tools_test");
   
  if( $_POST["pocketmon"]!=NULL ) {
   $date = date("Y-m-d H:i:s");
   
   $this->model_tools_test->insert( $_POST["pocketmon"], $date );
   
   echo $date."에 ".$_POST["pocketmon"]." 획득에 성공하셨습니다.";
  }
  else {
   echo "포획한 포켓몬의 이름을 적어주세요.";
   
   return false;
  }    
 }
 
 
 public function pocketmon_out(){
  
  $this->load->model("model_tools_test");
  
  $pocketmon=$_POST["pocketmon"];
  $result=$this->model_tools_test->out($pocketmon);
  
  if($result->num_rows()==0) echo "소환 실패.";
  else echo $_POST["pocketmon"]." 소환 성공";
       
 }
}
?>