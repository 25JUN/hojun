<html>
 <head>
  <title>동적 폼 전송 1번 실습 문제 해답</title>
 
  <script>
   function right_copy(){
    document.getElementById( "right" ).value = document.getElementById( "left" ).value;
   }
 
   function left_copy(){
    document.getElementById( "left" ).value = document.getElementById( "right" ).value;
   }
  </script>
 </head>
 
 <body>
  <input type="text" id="left"> <button onclick="javascript:right_copy();">오른쪽으로 복사</button>
 
  <input type="text" id="right"> <button onclick="javascript:left_copy();">왼쪽으로 복사</button>
 </body>
</html>

<br><br><br><br>응용문제 - 삼분할 로테이션<br><br>

<html>
 <head>
  <title>1번 문제</title>
 
   <script>
   function right_111(){
    document.getElementById( "222" ).value = document.getElementById( "111" ).value;
   }
   
   function right_222(){
    document.getElementById( "333" ).value = document.getElementById( "222" ).value;
   }
   
   function left_111(){
    document.getElementById( "111" ).value = document.getElementById( "333" ).value;
   }

   
    </script>
  </head>
 
 <body>
	<input type="text" id="111"> <button onclick="javascript:right_111();">오른쪽으로 복사</button>
	<input type="text" id="222"> <button onclick="javascript:right_222();">오른쪽으로 복사</button>
	<input type="text" id="333"> <button onclick="javascript:left_111();">오른쪽으로 복사</button>
 </body>
</html>