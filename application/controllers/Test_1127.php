<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 
class Test_1127 extends CI_Controller {

 public function form()
 {
  //폼 화면을 화면에 출력하는 메소드
     $this->load->view( "view_1127" );
 }



public function receive()
 {
 $this->load->library("form_validation");
 $this->form_validation->set_rules("n", "이름", "required|max_length[10]|min_length[2]");
 $this->form_validation->set_rules("i", "아이디", "required");
 $this->form_validation->set_rules("p", "비밀번호", "required|min_length[6]");
 $this->form_validation->set_rules("t", "전화번호", "required|numeric");
 $this->form_validation->set_rules("s", "성별", "required|in_list[남자,여자]");
 
 $year = $_POST['birth_year'];
 $month = $_POST['birth_month'];
 $day = $_POST['birth_date'];
 
 
 if ( $this->form_validation->run() ) {
  $this->load->model("model_tools");
  $this->model_tools->form($year, $month, $day);
  echo "회원가입 완료되셨습니다!";
 }
 else
 { echo "무언가 잘못 적으신게 있거나 또는 조건에 부합되지 않게 쓰셔서 회원가입이 실패되셨습니다!"; }
 
 }
}

 ?>