
<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 
class Test2 extends CI_Controller {
 
 public function form()
 {
  //폼 화면을 화면에 출력하는 메소드
     $this->load->view( "dynamic_form2" );
 }
 
 public function dynamic_form2()
 {
  //폼 화면이 보낸 post값을 처리하는 메소드
     if ( $_POST["msg"] == "야호" ) echo "잘했습니다.";
  else echo "못했습니다.";
 }
 
}