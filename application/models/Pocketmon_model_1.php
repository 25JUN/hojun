<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 
class Pocketmon_model_1 extends CI_model {
 
 public function insert()
 {
  $data= array(
   array(
    "name"     => "피카츄",
    "gender"   => "남",
    "birthday" => "1990-01-31"
   ),
    
   array(
    "name"     => "라이츄",
    "gender"   => "여",
    "birthday" => "1992-03-31"
   ),
    
   array(
    "name"     => "파이리",
    "gender"   => "남",
    "birthday" => "1970-05-02"
   ),
  );
 
  return $this->db->insert_batch( "pocketmon", $data );
 }
 
}
 
?>