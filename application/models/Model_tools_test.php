<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 
class Model_tools_test extends CI_Model {
 
 public function insert( $pocketmon, $date ){
  $data = array(
   'name'=>$pocketmon,
   'date'=>$date
  );
  
  $this->db->insert( 'pocketmon', $data );
 }
 
 public function out($pocketmon){
  
  $this->db->where("name",$pocketmon);
  
  $result=$this->db->get("pocketmon");
  return $result;
 }
}
 
?>