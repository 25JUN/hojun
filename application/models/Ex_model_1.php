<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 
class Ex_model_1 extends CI_model {
 
 public function insert()
 {
  $data= array(
   array(
    "id"       => "hong1234",
    "pw"       => "8o4bkg",
    "name"     => "홍길동",
    "gender"   => "남",
    "birthday" => "1990-01-31"
   ),
    
   array(
    "id"       => "sexysung",
    "pw"       => "87awjkdf",
    "name"     => "성춘향",
    "gender"   => "여",
    "birthday" => "1992-03-31"
   ),
    
   array(
    "id"       => "power70",
    "pw"       => "qxur8sda",
    "name"     => "변사또",
    "gender"   => "남",
    "birthday" => "1970-05-02"
   ),
    
   array(
    "id"       => "hanjo",
    "pw"       => "jk48fn4",
    "name"     => "한조",
    "gender"   => "남",
    "birthday" => "1984-10-18"
   ),
    
   array(
    "id"       => "widomaker",
    "pw"       => "38ewifh3",
    "name"     => "위도우",
    "gender"   => "여",
    "birthday" => "1986-06-27"
   ),
    
   array(
    "id"       => "dvadva",
    "pw"       => "k3f3ah",
    "name"     => "송하나",
    "gender"   => "여",
    "birthday" => "1994-06-03"
   ),
    
   array(
    "id"       => "jungkrat",
    "pw"       => "4ifha7f",
    "name"     => "정크렛",
    "gender"   => "남",
    "birthday" => "1975-11-11"
   )
  );
 
  return $this->db->insert_batch( "sql_practice", $data );
 }
 
}
 
?>